import os
import re
from bs4 import BeautifulSoup
from string import ascii_uppercase
import argparse
import pandas as pd
from multiprocessing import cpu_count, Pool
from glob import glob

from investing.utils import downloader
from investing.config import config

moneycontrol_data_dir = config.moneycontrol_data_dir
parallel_download = downloader.parallel_download

# Set up

def set_up():
    required_dirs = [moneycontrol_data_dir,
                     os.path.join(moneycontrol_data_dir, 'alphabets'),
                     os.path.join(moneycontrol_data_dir, 'stock_quote_pages')]

    for req_dir in required_dirs:
        if not os.path.exists(req_dir):
            print('Making directory at: {}'.format(req_dir))
            os.mkdir(req_dir)


def download_alphabet_pages(download_if_dst_exists=False):

    # Pages to be downloaded


    page_url = 'http://www.moneycontrol.com/india/stockpricequote/{}'

    alphabet_urls = [page_url.format(s) for s in ascii_uppercase]
    alphabet_urls.append(page_url.format('others'))


    # Location to save downloaded pages

    dst_dir = os.path.join(moneycontrol_data_dir, 'alphabets')
    dst_file_names = [os.path.join(dst_dir, s + '.html')
                      for s in ascii_uppercase]
    dst_file_names.append(os.path.join(dst_dir, 'others.html'))

    # Download pages
    parallel_download(alphabet_urls,
                      dst_file_names,
                      nthreads=2,
                      download_if_dst_exists=download_if_dst_exists)

    return dst_file_names


def extract_stock_quote_page_urls(alphabet_page_fnames):

    regex = re.compile(
        r'http://www.moneycontrol.com/india/stockpricequote/(\w+)/(\w+)/(\w+)')


    def parse_urls(file_name):

        soup = BeautifulSoup(open(file_name).read(),
                             'html5lib', from_encoding="utf-8")
        atags = soup.find_all('a')
        urls = [atag.get('href', None) for atag in atags]
        titles = [atag.get('title', None) for atag in atags]

        req_data1 = [(regex.match(url), title)
                     for (url, title) in zip(urls, titles)
                     if url and regex.match(url)]

        req_data2 = [(url.group(1), url.group(2), url.group(3), title,
                      url.group(0)) for (url, title) in req_data1]

        return req_data2


    industry_urlcompany_code_company_url_records = []

    for alphabet_page_fname in alphabet_page_fnames:
        records = parse_urls(alphabet_page_fname)
        industry_urlcompany_code_company_url_records.extend(records)


    df = pd.DataFrame.from_records(
        industry_urlcompany_code_company_url_records)
    df.columns = ['mc_industry', 'mc_urlcompany', 'mc_code', 'mc_company',
                  'mc_stock_quote_page_url']

    return df


def download_stock_quote_pages(mc_stock_quote_page_urls,
                               download_if_dst_exists=False):

    def dst_file_name(s):
        file_name = s.strip().split('/')[-1]
        return os.path.join(dst_dir, file_name+'.html')

    dst_dir = os.path.join(moneycontrol_data_dir, 'stock_quote_pages')

    dst_file_names = [dst_file_name(s) for s in mc_stock_quote_page_urls]

    parallel_download(mc_stock_quote_page_urls, dst_file_names,
                      nthreads=64,
                      download_if_dst_exists=download_if_dst_exists)

    return dst_file_names


def parse_stock_quote_page(fname):

    # Regular expressions to extract required information
    re_bsecode = re.compile(r'BSE:\s?(\d{6})')
    re_nsecode = re.compile(r'>\s?NSE:\s?(\w+)\s?<')
    re_isincode = re.compile(r'ISIN:\s?(IN\w{10})')
    re_hisdatacsv = re.compile(r'/tech_charts/bse/his/(\w+).csv')
    re_codes = [re_bsecode, re_nsecode, re_isincode, re_hisdatacsv]

    with open(fname, encoding='utf-8', errors='ignore') as f:
        s = f.read()
        match_objs = [regex.search(s) for regex in re_codes]
        bsecode, nsecode, isincode, hisdatacsv = \
                [mo.group(1) if mo else None for mo in match_objs]
        url_s = 'http://www.moneycontrol.com/tech_charts/bse/his/{}.csv'
        if hisdatacsv:
            historical_data_csv_url = url_s.format(hisdatacsv)
        else:
            historical_data_csv_url = ''

    return (bsecode, nsecode, isincode, historical_data_csv_url)


def add_bsecode_nsecode_hisdatacsvurl(df, local_stock_quote_pages):

    ncpus = 4*cpu_count()
    p = Pool(ncpus)

    codes = p.map(parse_stock_quote_page, local_stock_quote_pages)

    # Add column of bsecodes to the existing dataframe

    df['bsecode'], df['nsecode'], df['isincode'], \
            df['mc_historical_data_csv_url'] = zip(*codes)

    return df

if __name__ == '__main__':

    set_up()
    local_alphabet_pages = download_alphabet_pages()
    df_mc = extract_stock_quote_page_urls(local_alphabet_pages)
    mc_codes = df_mc.mc_code
    local_stock_quote_pages = [os.path.join(moneycontrol_data_dir,
                                            'stock_quote_pages',
                                            mc_code+'.html')
                               for mc_code in mc_codes]
    df_mc = add_bsecode_nsecode_hisdatacsvurl(
        df_mc, local_stock_quote_pages)

    # Write them to file
    mctable = os.path.join(moneycontrol_data_dir,
                           'mctable.csv')
    df_mc.to_csv(mctable, index=False)
