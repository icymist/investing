#! /usr/bin/env python

"""
Script to extract url of all companies stock quote pages from alphabet pages.
The extracted urls are writeen to stock_quote_page_urls_1.csv.
These urls are then used to download the stock quote pages.
"""

import os
import re
from string import ascii_uppercase
from bs4 import BeautifulSoup
from investing.config import moneycontrol_data_dir
import pandas as pd

alphabet_pages_dir = os.path.join(moneycontrol_data_dir, 'alphabets')
mc_urls_file_name = os.path.join(moneycontrol_data_dir, 'stock_quote_page_urls_1.csv')
regex = re.compile(r'http://www.moneycontrol.com/india/stockpricequote/(\w+)/(\w+)/(\w+)')


def get_urls(file_name):
    soup = BeautifulSoup(open(file_name).read(), 'html5lib', from_encoding="utf-8")
    atags = soup.find_all('a')
    urls = [atag.get('href', None) for atag in atags]
    titles = [atag.get('title', None) for atag in atags]

    req_data1 = [(regex.match(url), title)
            for (url, title) in zip(urls, titles)
            if url and regex.match(url)]

    req_data2 = [(url.group(1), url.group(2), url.group(3), title, url.group(0))
            for (url, title) in req_data1]

    return req_data2


def run():
    industry_urlcompany_code_company_url_records = []
    alphabet_page_files = [os.path.join(alphabet_pages_dir, s+'.html')
            for s in ascii_uppercase]
    alphabet_page_files += [os.path.join(alphabet_pages_dir, 'others.html')]
    for alphabet_page_file in alphabet_page_files:
        records = get_urls(alphabet_page_file)
        industry_urlcompany_code_company_url_records.extend(records)

    df = pd.DataFrame.from_records(industry_urlcompany_code_company_url_records)
    df.columns = ['mc_industry', 'mc_urlcompany', 'mc_code', 'mc_company',
            'mc_stock_quote_page_url']
    df.to_csv(mc_urls_file_name, index=False)

if __name__ == '__main__':
    run()
