#! /usr/bin/env python

"""
Script to extract bsecodes from downloaded moneycontrol stock quote pages to
make a csv file with bsecode, moneycontrol_stock_quote_page_url

Files to be parsed are read from preliminary stock_quote_page_urls_1 file where
only the urls are listed and no bsecodes
"""

import os, re
import pandas as pd
from glob import glob
from multiprocessing import cpu_count, Pool
from investing.config import moneycontrol_data_dir

ncpus = 4*cpu_count()

# Regular expression to extract the bsecode
re_bsecode = re.compile(r'BSE:\s?(\d{6})')
re_nsecode = re.compile(r'>\s?NSE:\s?(\w+)\s?<')
re_isincode = re.compile(r'ISIN:\s?(IN\w{10})')
re_hisdatacsv = re.compile(r'/tech_charts/bse/his/(\w+).csv')
re_codes = [re_bsecode, re_nsecode, re_isincode, re_hisdatacsv]

def get_stock_quote_page_file(stock_quote_page_url):
    head = os.path.join(moneycontrol_data_dir, 'stock_quote_pages')
    tail = stock_quote_page_url.split('/')[-1] + '.html'
    return os.path.join(head, tail)

def get_codes(fname):
    with open(fname, encoding='utf-8', errors='ignore') as f:
        s = f.read()
        match_objs = [regex.search(s) for regex in re_codes]
        bsecode, nsecode, isincode, hisdatacsv = \
                [mo.group(1) if mo else None for mo in match_objs]
        url_s = 'http://www.moneycontrol.com/tech_charts/bse/his/{}.csv'
        if hisdatacsv:
            historical_data_csv_url = url_s.format(hisdatacsv)
        else:
            historical_data_csv_url = ''
        return (bsecode, nsecode, isincode, historical_data_csv_url)


def get_bsecode(f):
    if os.path.exists(f):
        matches = re_bsecode.findall(open(f, encoding='utf-8', errors='ignore').read())
    else:
        print('%s does not exist' % (f))
        matches = None
    if matches:
        return matches[0]#.split()[-1]
    else:
        return None

def run():
    # Get the urls from the stock_quote_page_urls_1 file
    df = pd.read_csv(os.path.join(moneycontrol_data_dir, 'stock_quote_page_urls_1.csv'))

    stock_quote_page_files = \
            df['mc_stock_quote_page_url'].apply(
                    get_stock_quote_page_file).values

    p = Pool(ncpus)

    codes = p.map(get_codes, stock_quote_page_files)

    # Add column of bsecodes to the existing dataframe
    df['bsecode'], df['nsecode'], df['isincode'], \
            df['mc_historical_data_csv_url'] = zip(*codes)

    # Write them to file
    stock_quote_urls_file = os.path.join(moneycontrol_data_dir,
                                         'stock_quote_page_urls.csv')
    df.to_csv(stock_quote_urls_file, index=False)


if __name__ == '__main__':
    run()
